import pytest
from abc import ABCMeta, abstractmethod
from typing import List

from gluer import Gluer, Container, ServiceNotRegistered, Unannotated


@pytest.fixture
def gluer():
    return Gluer()


def test_resolving_simple(gluer):
    class A:
        pass

    gluer.register(A)

    container = gluer.build()

    instance = container.resolve(A)
    assert isinstance(instance, A)


def test_single_dependency(gluer):
    class A:
        pass
    class B:
        def __init__(self, a: A):
            self.a = a
    
    gluer.register(A)
    gluer.register(B)

    container = gluer.build()

    instance = container.resolve(B)

    assert isinstance(instance, B)
    assert isinstance(instance.a, A)


def test_nested_dependency(gluer):
    class A:
        pass
    class B:
        def __init__(self, a: A):
            self.a = a
    class C:
        def __init__(self, b: B):
            self.b = b
    
    gluer.register(A)
    gluer.register(B)
    gluer.register(C)

    container = gluer.build()
    instance = container.resolve(C)

    assert isinstance(instance, C)
    assert isinstance(instance.b, B)
    assert isinstance(instance.b.a, A)


def test_skipping_default_parameter(gluer):
    class A:
        pass
    class B:
        def __init__(self, a: A = None):
            self.a = a

    gluer.register(A)
    gluer.register(B)

    container = gluer.build()
    instance = container.resolve(B)

    assert isinstance(instance, B)
    assert instance.a is None

def test_resolving_unregistered_raises(gluer):
    class A:
        pass

    container = gluer.build()
    
    with pytest.raises(ServiceNotRegistered):
        instance = container.resolve(A)


def test_resolving_with_unannotated_raises(gluer):
    class A:
        def __init__(self, a):
            pass

    with pytest.raises(Unannotated):
        gluer.register(A)


def test_resolving_by_service(gluer):
    class ServiceA:
        pass
    class ServiceB:
        pass

    class A(ServiceA, ServiceB):
        pass

    gluer.register(A).As(ServiceA, ServiceB)

    container = gluer.build()

    assert isinstance(container.resolve(ServiceA), A)
    assert isinstance(container.resolve(ServiceB), A)


def test_getting_all_services(gluer):
    class Service:
        __meta__ = ABCMeta
        
        @abstractmethod
        def data(self):
            pass

    class FirstProvider(Service):
        def data(self):
            return "first"

    class SecondProvider(Service):
        def data(self):
            return "second"

    class Client:
        def __init__(self, services: List[Service]):
            self.services = services

    gluer.register(FirstProvider).As(Service)
    gluer.register(SecondProvider).As(Service)
    gluer.register(Client)

    container = gluer.build()
    instance = container.resolve(Client)

    assert len(instance.services) == 2

    first, second = instance.services
    assert isinstance(first, FirstProvider)
    assert isinstance(second, SecondProvider)


def test_getting_single_instance(gluer):
    class A:
        pass
    gluer.register(A).single_instance()
    
    container = gluer.build()

    ins1 = container.resolve(A)
    ins2 = container.resolve(A)

    assert ins1 is ins2


def test_registering_function(gluer):
    class A:
        pass
    class B:
        def __init__(self, a: A):
            self.a = a

    def factory(a: A) -> B:
        return B(a)

    gluer.register(A)
    gluer.register(factory)

    container = gluer.build()
    result = container.resolve(B)

    assert isinstance(result, B)
    assert isinstance(result.a, A)


def test_registering_function_without_annotation_throws(gluer):
    class A:
        pass
    def factory():
        return A()

    with pytest.raises(Unannotated):
        gluer.register(factory)


def test_registering_with_context(gluer):
    class A:
        pass
    class B:
        def __init__(self, a: A):
            self.a = a

    def factory(context: Container) -> B:
        inner = context.resolve(A)
        return B(inner)

    gluer.register(A)
    gluer.register(factory)

    container = gluer.build()
    result = container.resolve(B)

    assert isinstance(result, B)
    assert isinstance(result.a, A)


def test_registering_as_all_parent_classes(gluer):
    class A:
        pass
    class B:
        pass
    class C(A, B):
        pass

    gluer.register(C).as_parents()
    container = gluer.build()

    assert isinstance(container.resolve(A), C)
    assert isinstance(container.resolve(B), C)


def test_registering_instance(gluer):
    class A:
        pass
    instance = A()
    
    gluer.register_instance(instance)
    container = gluer.build()

    assert container.resolve(A) is instance
    assert container.resolve(A) is instance


# classes defined in locals cant be reached by string annotations
class A:
    pass
class B:
    def __init__(self, a: "A"):
        self.a = a

def test_string_annotation_gets_evaluated(gluer):
    gluer.register(A)
    gluer.register(B)

    container = gluer.build()
    instance = container.resolve(B)

    assert isinstance(instance.a, A)









