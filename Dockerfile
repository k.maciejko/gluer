FROM python:3.7

RUN pip install pytest

ADD . /gluer

CMD ["pytest", "/gluer"]
